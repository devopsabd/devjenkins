# DevOps (CI/CD) bo'yicha qisqacha qo'llanma misollar bilan

<img src="./images/diagram.jpeg" alt="Laravel Logo">

<br/>

# Minumum kerak bo’ladigan bilim va ko’nimalar

 <br/>
<br/>
<img src="./images/requirements.png" alt="Laravel Logo">
<br/>
<br/>
<!-- <ol>
    <br/>
    <li>Dasturlashdan habari bo’lishi. Kamida biron dasturlash tilidan habari bo’lishi maqsadga muvofiqdir. Chunki o’rganish jarayonida biror proyekt asosida ci/cd amalga oshiriladi. Shunda bu bilimlar asqotadi.
    </li>
    <br/>
    <li>Git dasturi haqida ma’lumot va bilimlarga ega bo’lishi. Kamida git “add”,”commit”, “push” buyruqlarini bilishi lozim.
    </li>
    <br/>
    <li>Source control tizimlari haqida ma'lumotga  ega bo’lishi. Mashhurlari Gitlab, Github va boshqalar.
    </li>
    <br/>
    <li>Linux operatsion tizimi haqida bilim. Terminalda ishlay olishi va eng ko’p ishlatilinadigan buyruqlarni bilishi lozim. Shell script yoza olsa juda yaxshi.</li>
    <br/>
    <li>Networking haqida bilim. Ip adress, Mac address, host, portlar. Network turlari bridge, host, NAT va boshqalar.
    </li>
    <br/>
    <li>Konteynerlash haqida bilim. Docker va VM. Konteyner boshqaruv(container orchertration) dastur yoki tizimlari. Masalan Kubernetes, Docker Compose, Dokcer Swarm va boshqalar.
    </li>
</ol> -->
<br/>
<h1 align=center><b>Qilinadigan ishlar</b></h1><br/>

<ol>
    <br/>
    <li>  
        <code><a href="https://www.git-scm.com/downloads"><b>Git dasturini o’rnatish</b></a></code>
    </li>
    <br/>
    <li>
        <code><a href="https://gitlab.com/users/sign_in"><b>Gitlab account ochish.</b></a></code>
    </li>
    <br/>
    <li>
        <code><a href="https://gitlab.com"><b>Gitlab bilan integratsiya.</b></a></code> Lokal komyuterimizda <a href="https://docs.github.com/en/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent">ssh</a> dasturi yordamida <code>public</code> va <code>private</code> key generatsiya qilamiz. Gitlab saytiga kirib <a href="https://gitlab.com/-/profile">edit profile</a> bo’limiga o’tamiz. Undan so’ng <a href="https://gitlab.com/-/profile/keys">SSH keys</a> bo’limiga o’tamiz. <code>Add new key</code> tugmani bosamiz va local komyuterga generatsiya qilingan <code>public key</code> ni joylaymiz va saqlaymiz. Bu holatda gitlab va bizni local kompyuterimiz o’rtasida ssh orqali aloqa o’rnatiladi. Bu xavfsizlik talablariga javob beradi.
        <br/>
        <br/>
        <img src="./images/addSSHgitlab.png" alt="Laravel Logo">
    </li>
    <br/>
    <li>
        <code><a href="https://gitlab.com/projects/new"><b>Yangi repository yaratamiz.</b></a></code> Shu zahoti ikkita branch yaratamiz. <a>main</a> va <a>test</a>. Bu ikkalasidan biri  <code>default</code> branch bo’lishi lozim. Har ikkala branchni ham <code>protected</code> qilinadi. Main branchga push qilish imkoniyati checklanadi. Bu branchlar <code>protected</code> qilinishiga sabab. <code>CI/CD</code> pipelinelar shu branchlarda amalga oshiriladi. Biz qo’shadigan <code>CI/CD</code> variable lar ham faqat <code>protected</code> branchlarda mavjud bo’ladi.
        <br/>
        <br/>
        <img src="./images/createBranches.png" /></li>
        <br/>
    <li>
        Biron ixtiyoriy ishlaydigan web dastur tayyorlab olish. U frontend bo’lsin yoki backend farqi yo’q. <code>Github</code> yoki <code>Gitlab</code> dan tayyor proyektni ko’chirib olsa ham bo’ladi. Yoki <a href="https://gitlab.com/devopsabd/devopsjourney">bu loyihani </a> yuklab olinsa ham bo'ladi.
    </li>
    <br/>
    <li>
        Gitlabdan o'zimiz yaratgan loyihamizni <code>ssh orqali clone</code> qilamiz. Misol uchun.
        <br/>
        <br/>
        So'ngra tayyorlab qo'ygan loyihamaizni kodlarini yangi loyihamizni ichiga joylaymiz. Va nihoya loyihamiz `<code>`CI/CD`</code>`ni boshlash uchun minimum talablarga javob beradi.
    </li>
    <li>
        <code><a href="https://www.docker.com"><b>Endi docker bilan ishlashni boshlaymiz.</b></a></code>
        <br/>
        <br/>
        <img src="./images/docker-info.png" alt="Laravel Logo">
        <br/>
        <br/>
        <img src="./images/docker-container-info.png">
        <br/>
        <br/>
        <img src="./images/docker-image-info.png">
        <br/>
        <br/>
        <img src="./images/dockerfile-info.png">
        <br/>
        <br/>
    </li>
    <br/>
    <li>
        Misol uchun frontend dasturni ko'rib chiqamiz.
        <code><b>Dockerfile</b></code> quyidagi holatda. Dasturimizni image ikki stage da quriladi. Birinchisi <code>node image</code>  yordamida dasturni <code>build</code> qilib olamiz. Buning uchun birinchi navbatda node ichida <code>/app</code> papkasini yaratamiz va ishnishu yerda davom ettiramiz. Loyiha papkasidagi <code>package.json va package-lock.json</code>larni nusxalaymiz. Keyinchalik loyiha paketlari <code>node_modules</code>ni <code>npm install</code> orqali yaratamiz. Va loyihani qolgan kodlarni nusxalaymiz. Shundan so'ng <code>npm run build</code> ishga tushiriladi. Loyihamiz build qilingandan keyin bizga uni ishga tushirish uchun <code>node</code> kerak bo'lmaydi. Bizga <code>nginx</code> kerak bo'ladi. Shu sababli biz <code>loyihamiz docker image</code>ni qurishda node da build qilingan papkani nginx ichiga joylab qo'yamiz xolos.
        <br/>
        <br/>
        <img src="./images/docker.png" alt="Laravel Logo">
        <br/>
        <br/>
    </li>
    <br/>
    <li>
        Yuqorida aytilganidek <code>docker compose</code> containerlarni boshqarishda bizga qulaylik beradi. Docker compose <code>port mapping</code>, <code>volume</code> yaratish, <code>network</code> yaratish va containerlarga biriktirish. Containerlar orasida o'zaro aloqani yo'lga qo'yish va shunga o'xshash amallar. Bu yerda <code>$CI_REGISTRY_IMAGE</code> gitlab <code>container registry</code> manzil bo'lib, <code>build qilingan docker image</code>miz qayerga <code>push</code> aniqlab beradi.  
        <br/>
        <br/>
        <img src="./images/compose.png" alt="Laravel Logo">
        <br/>
        <br/>
    </li>
    <li>
        Dasturimiz <code>http request</code>larga javob bera olishi uchun <code>nginx</code>ni sozlashimiz kerak bo'ladi. Quyida <code>nginx</code> sozlamasi
        <br/>
        <br/>
        <img src="./images/nginx.png" alt="Laravel Logo">
        <br/>
        <br/>
    </li>
    <li>
        Endi <code>CI/CD</code> bilan shug'ullanishni boshlasak ham bo'ladi. Hozirgi dasturimizda biz <code>Gitlab CI</code>dan foydalanamiz. SHu sababli <code>pipeline</code>larni <code>.gitlab-ci.yml</code> faylda yozamiz. Gitlabni o'zi bu faylni aniqlab ishga tushirib yuboradi. Quyida loyihamizni <code>.gitlab-ci.yml</code> fayli ko'rsatilgan. 
        <br/>
        <br/>
        <img src="./images/gitlab-cicd.png" alt="Laravel Logo">
        <br/>
        <br/>
        Keling endi buni tahlil qilamiz. Eng tepadagi <code>stages</code> qismida bajariladigan <code>job</code>(amallar) ketma-ketligini ifodalanadi. Bizning loyihada faqat job<code>(build, deploy)</code>dan iborat. Keyinchalik <code>test va boshqa joblar</code> qo'shilishi mumkin. Har bitta job alohida <code>docker container</code>da ishga tushiriladi. Job tugagach bu container o'chirib tashlanadi. Containerlar qaysi <code>docker image asosida</code> ishga tushirilishini belgilab ketishimiz mumkin. Default holatda <code>ruby image</code> asosida container ishga tushiriladi.
        Endi <code>pipeline</code>nimizga qaytadigan bo'lsak.
        <br />
        <br />
    </li>
    <li>
        1-<code>build</code> job ishga tushadi. U <code>docker:24.0.5</code> image asosida va <code>docker:24.0.5-dind</code>(docker-in-docker) service asosida ishga tushiriladi. Bu degani <code>docker container ichida docker o'rnatilgan bo'ladi</code>. Sabab job(container) ichida biz <code>dasturimizni docker image</code>ni build qilib so'ngra <code>image registry</code>ga <code>push</code> qilish uchun. Bunda faqat <code>test branch</code>ga kod yuklanganda job ishga tushadi. 
        <br/>
        <br/>
        <img src="./images/ci-build.png" alt="Laravel Logo">
        <br/>
        <br/>
        <code>script</code> qismida asosiy job qiladigan ish yoziladi. Bizni loyihamizda birinchi <code>gitlab container registry</code>dan foydalanildi. Chunki bir qancha <code>private repository</code> yaratsa bo'ladi. <code>DockerHub</code>da esa faqat bitta <code>private repository</code> yaratish imkoniyati bor. Undan kopi <code>pullik</code>. Keyin <code>docker compose build</code> orqali loyihamizni docker imagei build qilinmoqda va keyin <code>docker compose push</code> buyruq orqali build qilingan docker image <code>gitlab image registry</code>ga yuklanmoqda. 
        <br />
        CI/CD job yakunlangandan keyin biron ish qilmoqchi bo'lsak <code>after_script</code> qismda amalga oshirishimiz mumkin bo'ladi. Bizning loyihamizda <code>build</code> job natijasini <code>telegramga botga jo'natish</code> yo'lga qo'yilgan. <code>docker:24.0.5</code> image <code>alpine linux</code> ustiga qurilganligi va default holatda <code>curl</code> o'rnatilmaganligi uchun uni o'rnatib olamiz(<code>apk add curl</code>). Notifikatsiya uchun alohida <code>shell script</code> tayyorlaymiz. 
        <br/>
        <br/>
        <img src="./images/shell.png" alt="Laravel Logo">
        <br/>
        <br/>
        Jobda o'sha faylni <code>executable(chmod +x gitlab-notif.sh)</code> qilib olamiz va ishga tushiramiz(<code>sh gitlab-notif.sh</code>). Bu scriptimiz <code>telegram bot token</code>, <code>bot user id</code>lari va job haqidagi ma'lumotlarni <code>environment variables</code>dan oladi va botga xabar jo'natadi.
    </li>
    <br/>
    <li>
        2.<code>deploy</code> job ishga tushadi.
        <br/>
        <br/>
        <img src="./images/ci-deploy.png" alt="Laravel Logo">
        <br/>
        <br/>
        Bu job <code>default ruby image container asosida ishga tushiriladi</code>(Bu hech qanday zarari yo'q. O'zimiz tanlashimiz mumkin <code>docker image</code>ni). Biz endi <code>remote server</code>imizga ulanishimiz kerak. Shundan so'ng <code>build qilingan image</code> va <code>yange kod</code>larni serverimizga yuklab olishimiz mumkin bo'ladi. Shu ishlarni <code>script</code> qismida yozamiz.
        <br />
        <br />
        <code><b>Muhim.</b></code>Biz serverga <code>ssh key</code> orqali ulanishimiz kerak. Buni amalga oshirish uchun bir nechta ish qilishimiz kerak. Esingizda bo'lsa <code>lokal kompyuterimizda</code> yangi <code>public,private ssh</code> keylarni generatsiya qilgan edik. Gitlab bilan tog'ridan-to'g'ri ulanish uchun <code>public ssh key</code>ni gitlab accountga qo'shgan edik. Endi navbati bilan quyidagi ishlarni amalga oshiramiz.
        <br />
        <br />
        <b>1-</b>lokalimizdagi <code>public ssh key</code>ni serverdagi <code>.ssh</code>papka ichidagi <code>authorized_keys</code>ga qoshishimiz kerak bo'ladi. Shunda lokal kompyuterimizdan serverga ulansak bo'ladi <code>parolsiz</code>.
        <br/>
        <br/>
        <img src="./images/auth-keys.png" alt="Laravel Logo">
        <br/>
        <br/>
        Endi serverimizga <code>ssh key</code> orqali <code>parolsiz</code> ulana olamiz.
        <br/>
        Shu imkoniyatni <code>gitlab</code>ga taqdim qilishimiz kerak. Qachonki deploy job ishga tushganda <code>bizni o'rnimizga</code> serverimizga ulanib docker image va kodlarni serverga yuklab qo'yishi uchun. 
        <br />
        Ma'lum bo'ladiki lokaldagi <code>private ssh key</code> qaysi qurilmada bo'lsa o'sha qurilmadan serverga <code>parolsiz</code> ulanish mumkin bo'ladi . Biz shundan foydalanib gitlab orqali serverga <code>parolsiz ssh key</code> orqali ulanishni amalga oshirishimiz mumkin.
        <br />
        <b>2-</b>lokalimizdagi <code>private ssh key</code>ni gitlabga joylaymiz. Bunda private keyni quyidagicha joylaymiz.
        <br/>
        <br/>
        <img src="./images/variables.png" alt="Laravel Logo">
        <br/>
        <br/>
        Bu variable serverga ulanish paytida berib yuboriladi. Natijada bu buyrug'imiz serverni parolini so'ramasdan serverga muvafaqiyatli ulanadi.
        Bu yerda biz jobimizga <code>SSH key</code>ni o'qish(<code>read=400</code>) uchun(<code>chmod 400 $SSH_KEY ./</code>) permission(<code>ruxsat</code>) berdik. Bu amal job bajarilishidan oldin(<code>before_script</code>) amalga oshirilishi lozim.
        <br/>
        <br/>
        <img src="./images/key-ssh.png" alt="Laravel Logo">
        <br/>
        <br/>
        Hullas shu tariqa serverga ulanib olamiz. Endi oddiy terminalda qilinadigan ishlar ketma-ketligini yozib qo'ysak bas. Gitlab CI/CD environment variables yordamida <code>gitlab container registry</code>ga ulanamiz va <code>build job</code>da build qilinib registryga joylangan docker imagelarni <code>pull</code> qilamiz. Loyiha papkasiga o'tib kodlarni <code>pull</code> qilamiz.
        <br /> 
        Va nihoya <code>docker compose up -d</code> buyrug'i orqali <code>pull</code> qilingan yangi <code>docker image</code>larni qaytadan <code>container</code> qilib ishga tushirib yuboramiz. 
        <br />
        Job bajarililgach <code>telegram botga xabar</code> jo'natiladi <code>after_script</code> qismda (build jobdagi kabi).
        <br/>
        <br/>
        <img src="./images/run-shell-deploy.png" alt="Laravel Logo">
        <br/>
        <br/>
        Shu bilan maqolamiz poyoniga yetdi.
    </li>
    <br/>
        <br/>
        <img src="./images/conclusion.png" alt="Laravel Logo">
        <br/>
        <br/>
</ol>
